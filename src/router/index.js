import { createRouter, createWebHistory } from "vue-router";
import ProjectDetails from "../views/ProjectDetails.vue";
import Projects from "../views/Projects.vue";
import Courses from "../views/Courses.vue";
import About from "../views/About.vue";
import CourseDetails from "../views/CourseDetails.vue";

const routes = [
  {
    path: "/",
    name: "Courses",
    component: Courses
  },
  {
    path: "/about",
    name: "About",
    component: About
  },
  {
    path: '/courses/:id',
    name: 'CourseDetails',
    props:true,
    component: CourseDetails
  },
  {
    path: "/projects",
    name: "Projects",
    component: Projects
  },
  {
    path: '/projects/:id',
    name: 'ProjectDetails',
    props:true,
    component: ProjectDetails
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;
